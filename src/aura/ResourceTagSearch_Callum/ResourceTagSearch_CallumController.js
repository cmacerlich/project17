({  

	doInit : function(component, event, helper) {
        //TODO: Component should display list of resources matching the tags on the opportunity. Get list of resources from TagsSearch apex class.
        
        var recordId = component.get("v.recordId");
        var getResources = component.get("c.getResources");
        
        getResources.setParams({
            "recordId" : recordId
        });

        getResources.setCallback(this, function(response){

            var state = response.getState();

            if(state === "SUCCESS"){
                
                var resourceTagInfoMap = response.getReturnValue();

                if(resourceTagInfoMap !== null && resourceTagInfoMap !== '' && resourceTagInfoMap !== undefined){

                    //Create list of objects that have a Resource name and a list of Tag : (Number of Occurances).

                    var resourceTagInfoList = [];                       //Empty list to begin with.

                    for(var key in resourceTagInfoMap){
                        
                        var resourceName = key;
                        var tagInfoList = resourceTagInfoMap[key];
                        var tagOccuranceList = [];

                        for(var i = 0; i < tagInfoList.length; i++){
                            
                            var tagOccurance = tagInfoList[i].Tag + ' (' + tagInfoList[i].NumberOfOccurances + ')';
                            tagOccuranceList.push(tagOccurance);
                        }

                        var resourceTagInfo = {
                            "name" : resourceName,
                            "tagOccuranceList" : tagOccuranceList,
                            "numberOfTags" : tagOccuranceList.length
                        };

                        resourceTagInfoList.push(resourceTagInfo);
                    }

                    component.set("v.resourceList", resourceTagInfoList);
                    component.set("v.hasTags", true);                           //Set hasTags attribute to show resources.
                
                } else{
                    component.set("v.hasTags", false);                          //Set hasTags to false to prevent aura:iteration error.
                }

            } else{

                var errorToast = $A.get("e.force:showToast");
                var errorMessage = 'Error retrieving resources: ' + response.getError()[0].message;
                
                errorToast.setParams({
                    "mode" : "dismissible",
                    "message" : errorMessage,
                    "style" : "Error"
                });
                
                errorToast.fire();
            }
        });
        
        /*getResources.setCallback(this, function(response){
            
            var state = response.getState();
            console.log('Response state: ', state);
            if(state === "SUCCESS"){
                
                var resourceMap = response.getReturnValue();
                console.log('resourceMap: ', resourceMap);
                var resourceList = [];

                for(var key in resourceMap){
                    
                    var tags = resourceMap[key];
                    var tagsMap = new Map;

                    for(var i = 0; i < tags.length; i++){
                        
                        var tagName = tags[i];
                        var tagNumber = tagsMap[tagName];
                        if(tagNumber !== undefined){
                            tagNumber++;
                            tagsMap.set(tagName, tagNumber);
                        } else{
                            tagsMap.set(tagName, 1);
                        }
                    }

                    var tagKeys = tagsMap.entries();
                    console.log('tagKeys: ', tagKeys);
                    var tagsMapList = [];


                    /*
                    for(var i = 0; i < tagKeys.length; i ++){

                        var key = tagsMap.keys()[i];
                        var tagNumberPair = {
                            "tagName" : key,
                            "numberOfTimesTagged" : tagsMap[key].length
                        };

                        tagsMapList.push(tagNumberPair);

                    }
                    *//*

                    var resource = {
                        "name" : key,
                        "tags" : tags,
                        "numberOfTags" : tags.length,
                        "tagsMapKeySet" : tagsMap.keys(),
                        "tagsMapValues" : tagsMap.values()
                    };

                    resourceList.push(resource);
                }
                console.log('resourceList: ', resourceList);
                component.set("v.resourceList", resourceList);
                
            } else{
                
                //TODO: Move to common component for consistent error handling.
                var errorToast = $A.get("e.force:showToast");
                var errorMessage = 'Error retrieving resources: ' + response.getError()[0].message;
                
                errorToast.setParams({
                    "mode" : "dismissible",
                    "message" : errorMessage,
                    "style" : "Error"
                });
                
                errorToast.fire();
            }
        }); */
        
        $A.enqueueAction(getResources);
	}
})