public class TagSearch {

    public static List<String> searchResourcesByTag(String tags){
        
        List<String> oppTagList = tags.split(';');
         System.debug('test1 '+oppTagList);
         System.debug(oppTagList.size());
        
      Set<String> allResourceNames = new Set<String>();  
      
        For (String searchTerm :oppTagList){
            
            String searchTag = '%'+searchTerm+'%';
           
            
            //Dynamic SOQL query
            List<trailheadapp__User_Badge__c> listUserBadge = 
                 [SELECT Resource__r.Name 
                  FROM trailheadapp__User_Badge__c 
                  WHERE trailheadapp__Badge__r.trailheadapp__Tags__c LIKE :searchTag];
                  
           
            for(trailheadapp__User_Badge__c userBadge : listUserBadge){
                allResourceNames.add(userBadge.Resource__r.Name);
            }
        }
        System.debug('test2 '+allResourceNames);
       
        
        List<String> listResources = new List<String>();
        listResources.addAll(allResourceNames);
        
        System.debug('test3 '+listResources);
        
        return listResources;
        
    }
    
}