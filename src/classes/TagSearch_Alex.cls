public class TagSearch_Alex {
    
    //TODO: Display both Resource Name and the number of tags they are associated to.
    //TODO: V1: Display location. V2: Filter by location of opportunity.
    //TODO(ALEX): Provide link to resource from component.
    //TODO: Check resource is available (check utilisation).
    //TODO: Ability to remove resources the list.
    //TODO: Ability to add resources to the opportunity from component. Once they're are assigned to the opp, they should be removed from the list of resources.
    //TODO: Once opp complete, add tags from this against the resource.
    //TODO: Restrict component security to certain profile/role/permission set.

    @AuraEnabled
    public Static Map<String,List<ResourceTagInfo>> getResources(String recordId){
        
        if(recordId != null){
            Opportunity opp = [SELECT Id, Tags__c FROM Opportunity WHERE Id =:recordId LIMIT 1];

            if(opp.Tags__c != null){                                                                    //Adding null check for tags (if Opportunity doesn't have tags)

            Map<String,List<ResourceTagInfo>> resourcelist = searchResourcesByTag(opp.Tags__c);            
            return resourcelist;

            } else{

                return null;
            }
        }
        else{
            throw new AuraHandledException('No opportunity Id found.');
            return null;
        }
        
    }
    
    //public static Map<String,List<String>> searchResourcesByTag(String tags){
    public static Map<String,List<ResourceTagInfo>> searchResourcesByTag(String tags){
        /* Queries resources based on tags
		 * Takes tag paramater in the format of 'Lightning;Apex;App Lifecycle'
         * Returns names of resources */
        
        List<String> tagList = tags.split(';');
        
        System.debug('tagList size:' + tagList.size() + ' values: ' + tagList);
        
        String dynamicQueryString = 'SELECT Resource__c, Resource__r.Name, trailheadapp__Badge__r.trailheadapp__Tags__c FROM trailheadapp__User_Badge__c WHERE ';
        
        for(Integer i=0; i<tagList.size(); i++){														
            String tagQuery = '';
            if(i!=0){ 
                //If this isn't the first item in the list, we need to place an OR between tag queries
                tagQuery += ' OR ';
            }
            String searchTerm = tagList[i];
            tagQuery += 'trailheadapp__Badge__r.trailheadapp__Tags__c LIKE \'' + searchTerm + '\'';
            dynamicQueryString += tagQuery;
        }
        
        System.debug('dynamicQueryString: ' + dynamicQueryString);
        
        List<trailheadapp__User_Badge__c> userBadgeList = Database.query(dynamicQueryString);
        System.debug('userBadgeList.size(): ' + userBadgeList.size());
        
        Map<String,List<String>> mapResourceTags = new Map<String,List<String>>();
        
        for(trailheadapp__User_Badge__c userBadge : userBadgeList){
            String resourceName = userBadge.Resource__r.Name;
            List<String> resourceTagList = new List<String>();
            if(mapResourceTags.get(resourceName)!=NULL){
                resourceTagList = mapResourceTags.get(resourceName);
            }
            resourceTagList.addAll(userBadge.trailheadapp__Badge__r.trailheadapp__Tags__c.split(';'));
            mapResourceTags.put(resourceName,resourceTagList);
        }
        
        //Don't think this is being used anymore?
        Map<String,Integer> mapResourceTagCount = new Map<String,Integer>();
        for(String resourceName : mapResourceTags.keySet()){
            System.debug(resourceName + ' tagCount: ' + mapResourceTags.get(resourceName).size() + ' resourceTags: ' + mapResourceTags.get(resourceName));
            mapResourceTagCount.put(resourceName, mapResourceTags.get(resourceName).size());
        }
        
        List<String> listResources = new List<String>();
        listResources.addAll(mapResourceTagCount.keySet());
        System.debug('listResources: ' + listResources);

        //Trying new getResourceTagInfo method and seeing how this is returned.
        Map<String,List<ResourceTagInfo>> resourceTagInfoMap = getResourceTagInfo(mapResourceTags);
        System.debug('resourceTagInfoMap: ' + resourceTagInfoMap);
        
        return resourceTagInfoMap;
        //return mapResourceTags;
        
    }

    /*
     * Method to create a map of Resource Name and a list of ResourceTagInfo objects.
    */
    private static Map<String,List<ResourceTagInfo>> getResourceTagInfo(Map<String,List<String>> mapResourceTags){

        Map<String,List<ResourceTagInfo>> resourceTagInfoMap = new Map<String,List<ResourceTagInfo>>();

        for(String resource : mapResourceTags.keySet()){

            Map<String,Integer> tagOccuranceMap = new Map<String,Integer>();
            List<String> tagsList = mapResourceTags.get(resource);

            for(String tag : tagsList){
                
                if(tagOccuranceMap.get(tag) == null){

                    tagOccuranceMap.put(tag, 1);

                } else{
                    
                    Integer occuranceCount = tagOccuranceMap.get(tag);
                    occuranceCount++;
                    tagOccuranceMap.put(tag, occuranceCount);

                }
            }
            
            List<ResourceTagInfo> resourceTagInfoList = new List<ResourceTagInfo>();
            for(String tag : tagOccuranceMap.keySet()){

                ResourceTagInfo tagInfo = new ResourceTagInfo();
                tagInfo.Tag = tag;
                tagInfo.NumberOfOccurances = tagOccuranceMap.get(tag);

                resourceTagInfoList.add(tagInfo);
            }

            resourceTagInfoMap.put(resource, resourceTagInfoList);
        }

        return resourceTagInfoMap;

    }

    /*
     * Inner class to store information on the number of times a Resource has a matching tag.
    */
    public class ResourceTagInfo{

        @AuraEnabled
        Public String Tag {get;set;}

        @AuraEnabled
        Public Integer NumberOfOccurances {get;set;}

    }
    
}