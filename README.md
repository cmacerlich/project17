# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for storing all components associated with James' component for his 2017 internship. This was created to test using Bitbucket and SourceTree version control.

### How do I get set up? ###

* All development work should be completed in the Project17 sandbox. For access, please contact one of the administrators of this sandbox:
	* James Geddes
	* Alex Crossan
	* Callum MacErlich

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Callum MacErlich